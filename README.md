# Select2Translate - Translate text from your selection clipboard!

```
Usage: select2translate [options]
OPTIONS
	-h, --help	show usage instructions (this)
	-i, --instance	mozhi instance to use for translations (default: https://mozhi.aryak.me)
	-s, --source	source language to use (default: auto)
	-t, --target	target language to use (default: en)
	-g, --get-cmd	command used to retrieve selected text (default: xclip on xorg and wl-clipboard/wl-paste on wayland)
	-p, --put-cmd	command used to copy translated text (default: xclip on xorg and wl-clipboard/wl-paste on wayland)
```


Dependencies:
- bash
- [jq](https://github.com/jqlang/jq) 
- xclip or [wl-clipboard](https://github.com/bugaevc/wl-clipboard) (unless get/put commands manually specified)
- curl
- notify-send (part of libnotify)

<video src="/attachments/ba32bad6-2caa-4b67-bbd1-66deabb111b3" title="demo.mp4" controls></video>
