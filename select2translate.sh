#!/usr/bin/env bash

# defaults
instance="https://mozhi.aryak.me" # maybe I shouldn't set it?
# Google (default engine in most other cases) doesn't support multi-line in mozhi yet.
# Hence, DDG which has a similar amount of languages and accuracy (based on bing) is used.
engine="duckduckgo"
source="auto"
target="en"

if [[ ${WAYLAND_DISPLAY} != "" ]]; then
	select_get_cmd="wl-paste --primary"
	select_put_cmd="wl-copy"
# If WAYLAND_DISPLAY isn't set but DISPLAY is (ie. xorg)
elif [[ ${DISPLAY} != "" ]]; then
	select_get_cmd="xclip -o"
	select_put_cmd="xclip -i -selection clipboard"
fi

# https://www.baeldung.com/linux/bash-parse-command-line-arguments
VALID_ARGS=$(getopt -o i:e:s:t:g:p:h --long instance:,engine:,source:,target:,get-cmd:,put-cmd:,help -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -i | --instance)
		instance="$2"
        shift 2
        ;;
    -e | --engine)
		engine="$2"
        shift
        ;;
    -s | --source)
		source="$2"
        shift 2
        ;;
    -t | --target)
		target="$2"
        shift 2
        ;;
    -g | --get-cmd)
		select_get_cmd="$2"
        shift 2
        ;;
    -p | --put-cmd)
		select_put_cmd="$2"
        shift 2
        ;;
	? | -h | --help)
		echo "Select2Translate - Translate text from your selection clipboard!

Copyright 2023 Arya Kiran and other contributors

Usage: $(basename $0) [options]

OPTIONS
	-h, --help	show usage instructions (this)
	-i, --instance	mozhi instance to use for translations (default: https://mozhi.aryak.me)
	-s, --source	source language to use (default: auto)
	-t, --target	target language to use (default: en)
	-g, --get-cmd	command used to retrieve selected text (default: xclip on xorg and wl-clipboard/wl-paste on wayland)
	-p, --put-cmd	command used to copy translated text (default: xclip on xorg and wl-clipboard/wl-paste on wayland)
		"
		exit 1
		;;
	--) shift; 
        break 
        ;;
  esac
done

if [[ $select_get_cmd == "" ]] || [[ $select_put_cmd == "" ]]; then
	echo "Unsupported Desktop. Please set the selection retrieval command with --get-cmd and translation copy command with --put-cmd. Exiting.."
	exit 1
fi

text="$(${select_get_cmd})"
output="$(curl -s --data-urlencode 'text='"${text}" --data-urlencode 'engine='"${engine}" --data-urlencode 'from='"${source}" --data-urlencode 'to='"${target}" "${instance}"'/api/translate')"
# This --arg k is needed since jq cant parse keys with `-` in them (mozhi has the key as translated-text for backwards compat. with simplytranslate)
text="$(echo $output | jq -r --arg k "translated-text" '.[$k]')"
echo $text
lang="$(echo $output | jq -r '.detected')"
[[ ${lang} == "" ]] && lang="${target}"

printf "${text}" | $select_put_cmd
notify-send "Selected text translated" "The text you selected has been copied to clipboard\nTranslated Text: ${text}\nDetected Language: ${lang}"
